package com.playground.drawingar.model;

import android.support.annotation.ColorInt;
import android.util.Log;

import com.google.ar.core.Anchor;
import com.google.ar.core.Pose;
import com.google.ar.core.TrackingState;
import com.playground.drawingar.BiquadFilter;
import com.playground.drawingar.rendering.LineUtils;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

/**
 * Представление линии.
 */
public class Stroke {

    /**
     * Список точек, из которых состоит луч.
     */
    private ArrayList<Vector3f> points = new ArrayList<>();

    /**
     * Толшина линии.
     */
    private float lineWidth;

    /**
     * Цвет линии.
     */
    @ColorInt
    private int lineColor;

    /**
     * Биквадрический фильтр.
     */
    private BiquadFilter biquadFilter;

//    private Vector3f planeTranslation;

    private Anchor anchor;

    private Pose planePose;

    /**
     * Длина линии.
     */
    public float totalLength = 0;

    /**
     * Линия нарисована.
     */
    public boolean finished = false;

    /**
     * Конструктор - создание новой линии.
     *
     * @param lineColor Цвет линии.
     */
    public Stroke(@ColorInt int lineColor, Anchor anchor) {
        this.lineColor = lineColor;
        this.anchor = anchor;
        this.planePose = anchor.getPose();
    }

    /**
     * Добавляет точке в линию.
     *
     * @param point Точка, которую нужно добавить в линию
     */
    public void add(Vector3f point) {
        int s = points.size();

        if (s == 0) {
            // Prepare the biquad filter
            biquadFilter = new BiquadFilter(0.07f, 3);
            for (int i = 0; i < 1500; i++) {
                biquadFilter.update(point);
            }
        }

        // Filter the point
        point = biquadFilter.update(point);

        // Check distance, and only add if moved far enough
        if (s > 0) {
            Vector3f lastPoint = points.get(s - 1);

            Vector3f temp = new Vector3f();
            temp.sub(point, lastPoint);

            if (temp.length() < lineWidth / 10) {
                return;
            }
        }

        // Add the point
        points.add(point);

        // Cleanup vertices that are redundant
        if (s > 3) {
            float angle = calculateAngle(s - 2);
            // Remove points that have very low angle change
            if (angle < 0.05) {
                points.remove(s - 2);
            } else {
                subdivideSection(s - 3, 0.3f, 0);
            }
        }

        calculateTotalLength();
    }

    /**
     * Заканчивает создание линии.
     */
    public void finishStroke() {
        finished = true;

        // Calculate total distance traveled
        float dist = 0;

        Vector3f d = new Vector3f();
        for (int i = 0; i < points.size() - 1; i++) {
            d.sub(points.get(i), points.get(i + 1));
            dist += d.length();
        }

        // If line is very short, overwrite it
        if (dist < 0.01) {
            if (points.size() > 2) {
                Vector3f p1 = points.get(0);
                Vector3f p2 = points.get(points.size() - 1);

                points.clear();
                points.add(p1);
                points.add(p2);
            } else if (points.size() == 1) {
                Vector3f v = new Vector3f(points.get(0));
                v.y += 0.0005;
                points.add(v);
            }
        }
    }

    /**
     * Возвращает поворота точки.
     *
     * @param index Индекс точки в списке точек.
     * @return Угол поворота.
     */
    private float calculateAngle(int index) {
        Vector3f p1 = points.get(index - 1);
        Vector3f p2 = points.get(index);
        Vector3f p3 = points.get(index + 1);

        Vector3f n1 = new Vector3f();
        n1.sub(p2, p1);

        Vector3f n2 = new Vector3f();
        n2.sub(p3, p2);

        return n1.angle(n2);
    }

    /**
     * Вычисляет длину линии.
     */
    public void calculateTotalLength() {
        totalLength = 0;
        for (int i = 1; i < points.size(); i++) {
            Vector3f dist = new Vector3f(points.get(i));
            dist.sub(points.get(i - 1));
            totalLength += dist.length();
        }

    }

    private void subdivideSection(int s, float maxAngle, int iteration) {
        if (iteration == 6) {
            return;
        }

        Vector3f p1 = points.get(s);
        Vector3f p2 = points.get(s + 1);
        Vector3f p3 = points.get(s + 2);

        Vector3f n1 = new Vector3f();
        n1.sub(p2, p1);

        Vector3f n2 = new Vector3f();
        n2.sub(p3, p2);

        float angle = n1.angle(n2);

        // If angle is too big, add points
        if (angle > maxAngle) {
            n1.scale(0.5f);
            n2.scale(0.5f);
            n1.add(p1);
            n2.add(p2);

            points.add(s + 1, n1);
            points.add(s + 3, n2);

            subdivideSection(s + 2, maxAngle, iteration + 1);
            subdivideSection(s, maxAngle, iteration + 1);
        }
    }

    public boolean updatePosition() {
        if (anchor.getTrackingState() == TrackingState.TRACKING) {
            Pose newPose = anchor.getPose();
            Vector3f translation = new Vector3f();
            translation.sub(new Vector3f(newPose.getTranslation()),
                    new Vector3f(planePose.getTranslation()));
            if (translation.length() > 0.00001) {
//                Log.d("Stroke", "update: " + translation.length());
                
                for (int i = 0; i < points.size(); i++) {
                    Vector3f p = LineUtils.TransformPoint(points.get(i), planePose, newPose);
                    points.set(i, p);
                }
                planePose = newPose;
                return true;
            }
        }
        return false;
    }

    public boolean isTracking() {
        return anchor.getTrackingState() == TrackingState.TRACKING;
    }

    /**
     * Возвращает вектор с координатами точки линии по индексу.
     *
     * @param index Индекс нужной точки.
     * @return Вектор с координатами точки.
     */
    public Vector3f get(int index) {
        return points.get(index);
    }

    /**
     * Возвращает количество точек линии.
     *
     * @return Количество точек линии.
     */
    public int size() {
        return points.size();
    }

    /**
     * Возвращает список точек линии.
     *
     * @return Список точек линии.
     */
    @SuppressWarnings("unused")
    public List<Vector3f> getPoints() {
        return points;
    }

    /**
     * Возвращает толщину линии.
     *
     * @return Толщина линии.
     */
    @SuppressWarnings("unused")
    public float getLineWidth() {
        return lineWidth;
    }

    /**
     * Возвращает цвет линии.
     *
     * @return Цвет линии.
     */
    public int getLineColor() {
        return lineColor;
    }

    /**
     * Устанавливает толщину линии.
     *
     * @param lineWidth толщина линии
     */
    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
    }

    /**
     * Скопировать линию.
     *
     * @return Линия, которую нужно скопировать.
     */
    public Stroke copy() {
        Stroke copy = new Stroke(lineColor, anchor);
        copy.lineWidth = lineWidth;
        copy.points = new ArrayList<>(points);
        return copy;
    }
}

