package com.playground.drawingar.model;

import javax.vecmath.Vector3f;

/**
 * Представления луча или полупрямой.
 */
public class Ray {

    /**
     * Начало луча.
     */
    public final Vector3f origin;

    /**
     * Направление луча.
     */
    public final Vector3f direction;

    /**
     * Конструктор - создание нового луча.
     *
     * @param origin    Начало луча.
     * @param direction Направление луча.
     */
    public Ray(Vector3f origin, Vector3f direction) {
        this.origin = origin;
        this.direction = direction;
    }
}
