package com.playground.drawingar.rendering;

import com.google.ar.core.Camera;
import com.google.ar.core.Pose;

import javax.vecmath.Vector3f;

/**
 * Вспомогательный класс, реализующий методы для работы с объектами класса {@link Pose}.
 */
public class PoseUtils {

    /**
     * Повернуть объект к камере.
     *
     * @param camera         Камера.
     * @param objectToRotate Объект, который нужно повернуть.
     * @return Повернутый объект.
     */
    public static Pose rotatePoseToCamera(Camera camera, Pose objectToRotate) {
        Pose objToCameraPose = objectToRotate
                .extractTranslation()
                .inverse()
                .compose(camera.getPose());
        float[] objectToCamera = objToCameraPose.getTranslation();

        return objectToRotate.extractTranslation()
                .compose(rotateBetween(new float[] {0, 1, 0}, objectToCamera));
    }

    /**
     * Поворачивает объект с направления <code>fromRaw</code>
     * в направление <code>toRaw</code>.
     *
     * @param fromRaw Первоначальное направление объекта.
     * @param toRaw Конечное направление объекта.
     * @return Объект, хранящий конечное напраление.
     */
    private static Pose rotateBetween(float[] fromRaw, float[] toRaw) {
        Vector3f from = new Vector3f(fromRaw);
        from.normalize();
        Vector3f to = new Vector3f(toRaw);
        to.normalize();

        Vector3f rotation = new Vector3f();
        rotation.cross(from, to);
        rotation.normalize();
        float angle = from.angle(to);
        rotation.scale((float) Math.sin(angle/2.0f));
        return Pose.makeRotation(rotation.x, rotation.y, rotation.z, (float) Math.cos(angle/2.0f));
    }

}
