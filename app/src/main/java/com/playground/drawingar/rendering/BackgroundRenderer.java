package com.playground.drawingar.rendering;

import android.content.Context;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.support.annotation.NonNull;

import com.google.ar.core.Camera;
import com.google.ar.core.Coordinates2d;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.playground.drawingar.R;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Визуализатор видеопотока.
 */
public class BackgroundRenderer {

    private static final String TAG = BackgroundRenderer.class.getSimpleName();

    /**
     * Количество координат на каждую вершину.
     */
    private static final int COORDS_PER_VERTEX = 2;
    /**
     * Количество координат текстуры на каждую вершину.
     */
    private static final int TEXCOORDS_PER_VERTEX = 2;
    /**
     * Размер типа float в байтах.
     */
    private static final int FLOAT_SIZE = 4;

    /**
     * Буфер координат вершин.
     */
    private FloatBuffer mQuadCoords;
    /**
     * Буфер координат текстуры.
     */
    private FloatBuffer mQuadTexCoords;

    /**
     * Объект шейдерой программы.
     */
    private int mQuadProgram;

    private int mQuadPositionParam;
    private int mQuadTexCoordParam;
    /**
     * Идентификатор текстуры.
     */
    private int mTextureId = -1;

    /**
     * Возвращает идентификатор текстуры.
     *
     * @return Идентификатор текстуры.
     */
    public int getTextureId() {
        return mTextureId;
    }

    /**
     * Выделяет и инициализирует ресурсы OpenGL, необходимые для визуализатора видеопотока.
     * Должен быть вызван в потоке GL, а именно в
     * {@link GLSurfaceView.Renderer#onSurfaceCreated(GL10, EGLConfig)}.
     *
     * @param context Нужен для доступа к шейдеру.
     * @throws IOException Исключение ввода/вывода.
     */
    public void createOnGlThread(Context context) throws IOException {
        // Generate the background texture.
        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);
        mTextureId = textures[0];
        int textureTarget = GLES11Ext.GL_TEXTURE_EXTERNAL_OES;
        GLES20.glBindTexture(textureTarget, mTextureId);
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        ByteBuffer bbCoords = ByteBuffer.allocateDirect(QUAD_COORDS.length * FLOAT_SIZE);
        bbCoords.order(ByteOrder.nativeOrder());
        mQuadCoords = bbCoords.asFloatBuffer();
        mQuadCoords.put(QUAD_COORDS);
        mQuadCoords.position(0);

        ByteBuffer bbTexCoordsTransformed =
                ByteBuffer.allocateDirect(4 * TEXCOORDS_PER_VERTEX * FLOAT_SIZE);
        bbTexCoordsTransformed.order(ByteOrder.nativeOrder());
        mQuadTexCoords = bbTexCoordsTransformed.asFloatBuffer();

        int vertexShader = ShaderUtil.loadGLShader(
                TAG,
                context,
                GLES20.GL_VERTEX_SHADER,
                R.raw.screenquad_vert);
        int fragmentShader = ShaderUtil.loadGLShader(
                TAG,
                context,
                GLES20.GL_FRAGMENT_SHADER,
                R.raw.screenquad_frag);

        mQuadProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mQuadProgram, vertexShader);
        GLES20.glAttachShader(mQuadProgram, fragmentShader);
        GLES20.glLinkProgram(mQuadProgram);
        GLES20.glUseProgram(mQuadProgram);

        ShaderUtil.checkGLError(TAG, "Program creation");

        mQuadPositionParam = GLES20.glGetAttribLocation(mQuadProgram, "a_Position");
        mQuadTexCoordParam = GLES20.glGetAttribLocation(mQuadProgram, "a_TexCoord");

        ShaderUtil.checkGLError(TAG, "Program parameters");
    }

    /**
     * Рисует кадр видеопотока. Необходимо вызвать до отрисовки виртуальных объектов.
     *
     * @param frame Последний {@code Frame} возвращенный {@link Session#update()}.
     */
    public void draw(@NonNull Frame frame) {
        if (frame.hasDisplayGeometryChanged()) {
            frame.transformCoordinates2d(
                    Coordinates2d.OPENGL_NORMALIZED_DEVICE_COORDINATES,
                    mQuadCoords,
                    Coordinates2d.TEXTURE_NORMALIZED,
                    mQuadTexCoords);
        }

        if (frame.getTimestamp() == 0) {
            return;
        }

        draw();
    }

    /**
     * Рисует кадр видеопотока c конфигурированными ранее координатами
     * текстуры изоброжения {@link BackgroundRenderer#mQuadTexCoords}.
     */
    private void draw() {
        // Ensure position is rewound before use.
        mQuadTexCoords.position(0);

        // No need to test or write depth, the screen quad has arbitrary depth, and is expected
        // to be drawn first.
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthMask(false);

        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureId);

        GLES20.glUseProgram(mQuadProgram);

        // Set the vertex positions.
        GLES20.glVertexAttribPointer(
                mQuadPositionParam,
                COORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                0,
                mQuadCoords);

        // Set the texture coordinates.
        GLES20.glVertexAttribPointer(
                mQuadTexCoordParam,
                TEXCOORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                0,
                mQuadTexCoords);

        // Enable vertex arrays
        GLES20.glEnableVertexAttribArray(mQuadPositionParam);
        GLES20.glEnableVertexAttribArray(mQuadTexCoordParam);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

        // Disable vertex arrays
        GLES20.glDisableVertexAttribArray(mQuadPositionParam);
        GLES20.glDisableVertexAttribArray(mQuadTexCoordParam);

        // Restore the depth state for further drawing.
        GLES20.glDepthMask(true);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        ShaderUtil.checkGLError(TAG, "Draw");
    }

    /**
     * Координаты квадрата, на который будет накладываться изображение.
     */
    private static final float[] QUAD_COORDS = {
            -1.0f, -1.0f,
            -1.0f, +1.0f,
            +1.0f, -1.0f,
            +1.0f, +1.0f,
    };
}
