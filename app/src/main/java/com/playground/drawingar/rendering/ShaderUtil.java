package com.playground.drawingar.rendering;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Вспомогательный класс, реализующий методы для работы с OpenGL ES.
 */
class ShaderUtil {

    /**
     * Переводит текстовый фойл, сохраненный в виде ресурса, в шейдер OpenGL ES.
     *
     * @param tag     Тег.
     * @param context Нужен для доступа к ресурсам.
     * @param type    Тип шейдера.
     * @param resId   Идентификатор ресурса текстового файла, который будет переведен в шейдер.
     * @return Объект управления шейдером.
     */
    static int loadGLShader(String tag, Context context, int type, int resId) {
        String code = readRawTextFile(context, resId);
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, code);
        GLES20.glCompileShader(shader);

        // Get the compilation status.
        final int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

        // If the compilation failed, delete the shader.
        if (compileStatus[0] == 0) {
            Log.e(tag, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = 0;
        }

        if (shader == 0) {
            throw new RuntimeException("Error creating shader.");
        }

        return shader;
    }

    /**
     * Проверяет наличие ошибки внутри OpenGL ES.
     * Если ошибка найдена, печатает ее в лог.
     *
     * @param tag   Тег.
     * @param label Лейбл в случае ошибки.
     * @throws RuntimeException В случае OpenGL ошибки.
     */
    static void checkGLError(String tag, String label) {
        int error = GLES20.glGetError();
        if (error != GLES20.GL_NO_ERROR) {
            Log.e(tag, label + ": glError " + error);
            throw new RuntimeException(label + ": glError " + error);
        }
    }

    /**
     * Читает файл шейдера и возвращает ее текст.
     *
     * @param resId Идентификатор ресурса текстового файла, который будет переведен в шейдер.
     * @return Содержание файла шейдера или <code>null</code> в случае ошибки.
     */
    private static String readRawTextFile(Context context, int resId) {
        InputStream inputStream = context.getResources().openRawResource(resId);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
