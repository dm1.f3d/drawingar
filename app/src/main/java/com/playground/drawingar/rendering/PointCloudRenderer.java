package com.playground.drawingar.rendering;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import com.google.ar.core.PointCloud;
import com.playground.drawingar.R;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

/**
 * Визуализатор множества маркеров.
 */
public class PointCloudRenderer {

    private static final String TAG = PointCloudRenderer.class.getSimpleName();

    /**
     * Количество байт во float.
     */
    private static final int BYTES_PER_FLOAT = Float.SIZE / 8;
    /**
     * Количество компонент на одну точку.
     */
    private static final int FLOATS_PER_POINT = 4;
    /**
     * Количество байт на одну точку.
     */
    private static final int BYTES_PER_POINT = BYTES_PER_FLOAT * FLOATS_PER_POINT;
    /**
     * Первоначальный размер буфера точек.
     */
    private static final int INITIAL_BUFFER_POINTS = 1000;

    /**
     * Объект шейдерой программы.
     */
    private int mProgramName = 0;
    private int mPositionAttribute = 0;
    private int mColorUniform = 0;
    private int mModelViewProjectionUniform = 0;
    private int mPointSizeUniform = 0;


    private int vbo = 0;
    private int vboSize = 0;

    /**
     * Количество точек.
     */
    private int mNumPoints = 0;

    /**
     * Время последнего обновления облака точек для исключения
     * ситуации, когда облако точек еще не обновилось.
     */
    private long mLastTimestamp = 0;

    /**
     * Выделяет и инициализирует ресурсы OpenGL, необходимые для визуализатора множества маркеров.
     * Должен быть вызван в потоке GL, а именно в
     * {@link GLSurfaceView.Renderer#onSurfaceCreated(GL10, EGLConfig)}.
     *
     * @param context Нужен для доступа к шейдеру.
     * @throws IOException Исключение ввода/вывода.
     */
    public void createOnGlThread(Context context) throws IOException {
        ShaderUtil.checkGLError(TAG, "before create");

        int[] buffers = new int[1];
        GLES20.glGenBuffers(1, buffers, 0);
        vbo = buffers[0];
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo);

        vboSize = INITIAL_BUFFER_POINTS * BYTES_PER_POINT;
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vboSize, null, GLES20.GL_DYNAMIC_DRAW);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        ShaderUtil.checkGLError(TAG, "buffer alloc");

        int vertexShader = ShaderUtil.loadGLShader(
                TAG,
                context,
                GLES20.GL_VERTEX_SHADER,
                R.raw.point_cloud_vert);
        int fragmentShader = ShaderUtil.loadGLShader(
                TAG,
                context,
                GLES20.GL_FRAGMENT_SHADER,
                R.raw.point_cloud_frag);

        mProgramName = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgramName, vertexShader);
        GLES20.glAttachShader(mProgramName, fragmentShader);
        GLES20.glLinkProgram(mProgramName);
        GLES20.glUseProgram(mProgramName);

        ShaderUtil.checkGLError(TAG, "program");

        mPositionAttribute = GLES20.glGetAttribLocation(mProgramName, "a_Position");
        mColorUniform = GLES20.glGetUniformLocation(mProgramName, "u_Color");
        mModelViewProjectionUniform = GLES20.glGetUniformLocation(mProgramName, "u_ModelViewProjection");
        mPointSizeUniform = GLES20.glGetUniformLocation(mProgramName, "u_PointSize");

        ShaderUtil.checkGLError(TAG, "program  params");
    }

    /**
     * Обновляет бувер OpenGL данными из множества точек.
     * Повторный выхов метода с одним и тем же множеством точек будет проигнорировано.
     *
     * @param cloud Множество точек.
     */
    public void update(PointCloud cloud) {
        if (cloud.getTimestamp() == mLastTimestamp) {
            return;
        }

        ShaderUtil.checkGLError(TAG, "before update");

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo);
        mLastTimestamp = cloud.getTimestamp();

        // If the VBO is not large enough to fit the new point cloud, resize it.
        mNumPoints = cloud.getPoints().remaining() / FLOATS_PER_POINT;
        if (mNumPoints * BYTES_PER_POINT > vboSize) {
            while (mNumPoints * BYTES_PER_POINT > vboSize) {
                vboSize *= 2;
            }
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vboSize, null, GLES20.GL_DYNAMIC_DRAW);
        }
        GLES20.glBufferSubData(
                GLES20.GL_ARRAY_BUFFER, 0, mNumPoints * BYTES_PER_POINT, cloud.getPoints());
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        ShaderUtil.checkGLError(TAG, "after update");
    }

    /**
     * Рисует множество точек. Точки заданы в мировом пространстве.
     *
     * @param cameraView        Матрица вида, которую можно получить с помощью {@link
     *                          com.google.ar.core.Camera#getViewMatrix(float[], int)}.
     * @param cameraPerspective Матрица вида, которую можно получить с помощью  {@link
     *                          com.google.ar.core.Camera#getProjectionMatrix(float[], int, float, float)}.
     */
    public void draw(float[] cameraView, float[] cameraPerspective) {
        float[] modelViewProjection = new float[16];
        Matrix.multiplyMM(modelViewProjection, 0, cameraPerspective, 0, cameraView, 0);

        ShaderUtil.checkGLError(TAG, "Before draw");

        GLES20.glUseProgram(mProgramName);
        GLES20.glEnableVertexAttribArray(mPositionAttribute);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo);
        GLES20.glVertexAttribPointer(mPositionAttribute, 4, GLES20.GL_FLOAT, false, BYTES_PER_POINT, 0);
        GLES20.glUniform4f(mColorUniform, 1.0f, 1.0f, 1.0f, 1.0f);
        GLES20.glUniformMatrix4fv(mModelViewProjectionUniform, 1, false, modelViewProjection, 0);
        GLES20.glUniform1f(mPointSizeUniform, 5.0f);

        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, mNumPoints);
        GLES20.glDisableVertexAttribArray(mPositionAttribute);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        ShaderUtil.checkGLError(TAG, "Draw");
    }
}
