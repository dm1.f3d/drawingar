package com.playground.drawingar.rendering;

import android.opengl.Matrix;
import android.util.Log;

import com.google.ar.core.Pose;
import com.playground.drawingar.model.Ray;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

/**
 * Вспомогательный класс, реализующий алгоритм проекции геометрический
 * фигур на объекты окружающей среды.
 */
public class LineUtils {

    /**
     * Возвращает координаты точки в мировом пространстве.
     *
     * @param touchPoint       Координаты точки на экране.
     * @param planePose        Рлоскость, на которую проецируется точка.
     * @param screenWidth      Ширина экрана.
     * @param screenHeight     Высота экрана.
     * @param projectionMatrix Матрица проекции.
     * @param viewMatrix       Матрица вида.
     * @return Вектор с координатами точки в мировом пространстве.
     */
    public static Vector3f GetWorldCoords(Vector2f touchPoint,
                                          Pose planePose,
                                          float screenWidth,
                                          float screenHeight,
                                          float[] projectionMatrix,
                                          float[] viewMatrix) {

        Ray touchRay = projectRay(new Vector2f(touchPoint),
                screenWidth,
                screenHeight,
                projectionMatrix,
                viewMatrix);

        Vector3f n = new Vector3f(planePose.getTransformedAxis(1, 1.0f));

        Vector3f m = new Vector3f();
        m.cross(touchRay.origin, touchRay.direction);

        Vector3f translation = new Vector3f(planePose.getTranslation());

        float e = translation.dot(n);

        float[] w = { e, 0, 0, n.x,
                      0, e, 0, n.y,
                      0, 0, e, n.z,
                      0, n.z, -n.y, 0,
                      -n.z, 0, n.x, 0,
                      n.y, -n.x, 0, 0};

        float[] line = { touchRay.direction.x,
                         touchRay.direction.y,
                         touchRay.direction.z,
                         m.x,
                         m.y,
                         m.z};

        float[] point = multiplyMV(w, line);

        Vector3f result = new Vector3f(point);
        result.scale(1 / point[3]);

        return result;
    }

    /**
     * Умножает матрицу 6 x 4 на вектор 6.
     *
     * @param mat Матрица 6 x 4.
     * @param vec Вектор 6.
     * @return Вектор 4.
     */
    private static float[] multiplyMV(float[] mat, float[] vec) {
        float[] result = new float[4];
        for (int i = 0; i < 4; i++) {
            float x = 0;
            for (int j = 0; j < 6; j++) {
                x += mat[i + 4 * j] * vec[j];
            }
            result[i] = x;
        }
        return result;
    }

    /**
     * Возвращает луч, брошенный с точки касания экрана.
     *
     * @param point        Координаты точки касания экрана.
     * @param viewportSize Размер экрана.
     * @param viewProjMtx  Матрица, полученная перемножением матриц вида и проекции.
     * @return Луч, представляющий собой направление и точку, через которую брошен луч.
     */
    private static Ray screenPointToRay(Vector2f point, Vector2f viewportSize, float[] viewProjMtx) {
        point.y = viewportSize.y - point.y;
        float x = point.x * 2.0F / viewportSize.x - 1.0F;
        float y = point.y * 2.0F / viewportSize.y - 1.0F;
        float[] farScreenPoint = new float[]{x, y, 1.0F, 1.0F};
        float[] nearScreenPoint = new float[]{x, y, -1.0F, 1.0F};
        float[] nearPlanePoint = new float[4];
        float[] farPlanePoint = new float[4];
        float[] invertedProjectionMatrix = new float[16];
        Matrix.setIdentityM(invertedProjectionMatrix, 0);
        Matrix.invertM(invertedProjectionMatrix, 0, viewProjMtx, 0);
        Matrix.multiplyMV(nearPlanePoint, 0, invertedProjectionMatrix, 0, nearScreenPoint, 0);
        Matrix.multiplyMV(farPlanePoint, 0, invertedProjectionMatrix, 0, farScreenPoint, 0);
        Vector3f direction = new Vector3f(farPlanePoint[0] / farPlanePoint[3],
                farPlanePoint[1] / farPlanePoint[3], farPlanePoint[2] / farPlanePoint[3]);
        Vector3f origin = new Vector3f(new Vector3f(nearPlanePoint[0] / nearPlanePoint[3],
                nearPlanePoint[1] / nearPlanePoint[3], nearPlanePoint[2] / nearPlanePoint[3]));
        direction.sub(origin);
        direction.normalize();
        return new Ray(origin, direction);
    }

    /**
     * Возвращает луч, брошенный с точки касания экрана.
     *
     * @param touchPoint       Координаты точки касания экрана.
     * @param screenWidth      Ширина экрана.
     * @param screenHeight     Высота экрана.
     * @param projectionMatrix Матрица проекции.
     * @param viewMatrix       Матрица вида.
     * @return Луч, представляющий собой направление и точку, через которую брошен луч.
     */
    private static Ray projectRay(Vector2f touchPoint, float screenWidth, float screenHeight,
                                  float[] projectionMatrix, float[] viewMatrix) {
        float[] viewProjMtx = new float[16];
        Matrix.multiplyMM(viewProjMtx, 0, projectionMatrix, 0, viewMatrix, 0);
        return screenPointToRay(touchPoint, new Vector2f(screenWidth, screenHeight),
                viewProjMtx);
    }

    public static Vector3f TransformPoint(Vector3f point, Pose fromAnchorPose, Pose toAnchorPose) {
        float[] position = new float[3];
        position[0] = point.x;
        position[1] = point.y;
        position[2] = point.z;

        position = fromAnchorPose.inverse().transformPoint(position);
        position = toAnchorPose.transformPoint(position);
        return new Vector3f(position[0], position[1], position[2]);
    }

}
