package com.playground.drawingar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.ar.core.ArCoreApk;
import com.playground.drawingar.helpers.PermissionHelper;

/**
 * Экран приветствия.
 * Главная задача этого экрана – предоставить основные разрешения для работы приложения.
 */
public class PermissionsActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Время задержки перед переход на новый экран в мс.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    /**
     * Перейти на главный экран.
     */
    private boolean mLaunchMainActivity = true;

    private boolean mUserRequestedARCoreInstall = true;

    /**
     * onCreate - часть жизненного цикла активности.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);
    }

    /**
     * onResume - часть жизненного цикла активности.
     */
    @Override
    protected void onResume() {
        super.onResume();

        int message = -1;
        Exception exception = null;
        try {
            switch (ArCoreApk.getInstance()
                    .requestInstall(this, mUserRequestedARCoreInstall)) {
                case INSTALLED:

                    break;
                case INSTALL_REQUESTED:
                    // Ensures next invocation of requestInstall() will either return
                    // INSTALLED or throw an exception.
                    mUserRequestedARCoreInstall = false;
                    // at this point, the activity is paused and user will go through
                    // installation process
                    return;
            }
        } catch (Exception e) {
            exception = e;
            message = getARCoreInstallErrorMessage(e);
        }

        // display possible ARCore error to user
        if (message >= 0) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            Log.e(TAG, "Exception creating session", exception);
            finish();
            return;
        }

        if (PermissionHelper.hasCameraPermission(this)) {
            startMainActivityDelayed();
        } else {
            PermissionHelper.requestCameraPermission(this);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mLaunchMainActivity = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (!PermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this,
                    "Camera permission is needed to run this application",
                    Toast.LENGTH_LONG).show();
            if (!PermissionHelper.shouldShowRequestPermissionRationale(this)) {
                PermissionHelper.launchPermissionSettings(this);
            }
            finish();
        }
    }

    /**
     * Открывает главный экран с задержкой по времени.
     */
    private void startMainActivityDelayed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mLaunchMainActivity) {
                    Intent intent = new Intent(PermissionsActivity.this,
                            MainActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}
