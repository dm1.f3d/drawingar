package com.playground.drawingar.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Вспомогательный класс для работы с разрешениями приложения.
 */
public final class PermissionHelper {

    private static final int CAMERA_PERMISSION_CODE = 0;
    /**
     * Идентификатор разрения на использование камеры телефона.
     */
    private static final String CAMERA_PERMISSION = Manifest.permission.CAMERA;

    /**
     * Проверяет наличие разрешения на использование камеры телефона.
     *
     * @param activity Нужно для доступа к методам системы.
     * @return <code>true</code> - если приложение имеет нужное разрешение.
     */
    public static boolean hasCameraPermission(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, CAMERA_PERMISSION)
                == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Делает запрас на разрешение использовать камеру телефона.
     *
     * @param activity Нужно для доступа к методам системы.
     */
    public static void requestCameraPermission(Activity activity) {
        ActivityCompat.requestPermissions(
                activity, new String[] {CAMERA_PERMISSION}, CAMERA_PERMISSION_CODE);
    }

    /**
     * Проверяет необходимость появления окна с обоснованием причины
     * необходимости получения нужного разрешения.
     *
     * @param activity Нужно для доступа к методам системы.
     * @return @return <code>true</code> означает, что нужно показать окно с обоснованием
     *                 причины необходимости получения нужного разрешения.
     */
    public static boolean shouldShowRequestPermissionRationale(Activity activity) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, CAMERA_PERMISSION);
    }

    /**
     * Запускает настройки с разрешениями приложения.
     *
     * @param activity Нужно для доступа к методам системы.
     */
    public static void launchPermissionSettings(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
        activity.startActivity(intent);
    }
}