package com.playground.drawingar.helpers;

import android.app.Activity;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Вспомогательный класс для работы со {@link Snackbar}.
 */
public final class SnackbarHelper {

    /**
     * Цвет заднего фона снекбара.
     */
    private static final int BACKGROUND_COLOR = 0xbf323232;
    /**
     * Объект для работы со {@link Snackbar}.
     */
    private Snackbar messageSnackbar;

    /**
     * dismiss behavior снекбара.
     */
    private enum DismissBehavior {
        /**
         * Hide dismiss behavior.
         */
        HIDE,
        /**
         * Show dismiss behavior.
         */
        SHOW,
        /**
         * Finish dismiss behavior.
         */
        FINISH
    }

    /**
     * Наибольшее число строк в снекбаре.
     */
    private int maxLines = 2;
    /**
     * Последнее выведенное сообщение.
     */
    private String lastMessage = "";

    /**
     * Возвращает состояние снекбара.
     *
     * @return <code>true</code> - если снекбар выведен на экран.
     */
    public boolean isShowing() {
        return messageSnackbar != null;
    }

    /**
     * Показывает снекбар с сообщением.
     *
     * @param activity Активность, на которой появится снекбар.
     * @param message  Сообщение, которое нужно вывести.
     */
    public void showMessage(Activity activity, String message) {
        if (!message.isEmpty() && (!isShowing() || !lastMessage.equals(message))) {
            lastMessage = message;
            show(activity, message, DismissBehavior.HIDE);
        }
    }

    /**
     * Показывает снекбар с сообщением и кнопкой для отмены.
     *
     * @param activity Активность, на которой появится снекбар.
     * @param message  Сообщение, которое нужно вывести.
     */
    public void showMessageWithDismiss(Activity activity, String message) {
        show(activity, message, DismissBehavior.SHOW);
    }

    /**
     * Показывает снекбар с сообщением об ошибке. Если пользователь нажал на отмену,
     * экран закрывается. Полезен для уведомления ошибок, с которыми дальнейшее использование
     * экрана невозможно.
     *
     * @param activity     Активность, на которой появится снекбар.
     * @param errorMessage Сообщение об ошибке, которое нужно вывести.
     */
    public void showError(Activity activity, String errorMessage) {
        show(activity, errorMessage, DismissBehavior.FINISH);
    }

    /**
     * Показывает ошибку.
     *
     * @param activity             Активность, на которой появится снекбар.
     * @param errorMessageResource Идентификатор ресурса с сообщением об ошибке
     */
    public void showError(Activity activity, int errorMessageResource) {
        show(activity, activity.getString(errorMessageResource), DismissBehavior.FINISH);
    }

    /**
     * Убирает снекбар с экрана, если он присутствует на экране.
     * Можно вызвать с любого потока.
     *
     * @param activity Активность, с которой нужно убрать снекбар.
     */
    public void hide(Activity activity) {
        if (!isShowing()) {
            return;
        }
        lastMessage = "";
        final Snackbar messageSnackbarToHide = messageSnackbar;
        messageSnackbar = null;
        activity.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        messageSnackbarToHide.dismiss();
                    }
                });
    }

    /**
     * Установить максимальное число строк.
     *
     * @param lines Число строк.
     */
    public void setMaxLines(int lines) {
        maxLines = lines;
    }

    /**
     * Общий метод для показа разного рода сообщения в снекбаре.
     *
     * @param activity        Активность, на которой появится снекбар.
     * @param message         Сообщение, которое нужно вывести на экран
     * @param dismissBehavior dismiss behavior
     */
    private void show(
            final Activity activity, final String message, final DismissBehavior dismissBehavior) {
        activity.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        messageSnackbar =
                                Snackbar.make(
                                        activity.findViewById(android.R.id.content),
                                        message,
                                        Snackbar.LENGTH_INDEFINITE);
                        messageSnackbar.getView().setBackgroundColor(BACKGROUND_COLOR);
                        if (dismissBehavior != DismissBehavior.HIDE) {
                            messageSnackbar.setAction(
                                    "Dismiss",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            messageSnackbar.dismiss();
                                        }
                                    });
                            if (dismissBehavior == DismissBehavior.FINISH) {
                                messageSnackbar.addCallback(
                                        new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                            @Override
                                            public void onDismissed(Snackbar transientBottomBar, int event) {
                                                super.onDismissed(transientBottomBar, event);
                                                activity.finish();
                                            }
                                        });
                            }
                        }
                        ((TextView)
                                messageSnackbar
                                        .getView()
                                        .findViewById(android.support.design.R.id.snackbar_text))
                                .setMaxLines(maxLines);
                        messageSnackbar.show();
                    }
                });
    }
}

