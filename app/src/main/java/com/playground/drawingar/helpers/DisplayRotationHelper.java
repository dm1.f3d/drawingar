package com.playground.drawingar.helpers;

import android.app.Activity;
import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.display.DisplayManager;
import android.hardware.display.DisplayManager.DisplayListener;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import com.google.ar.core.Session;

/**
 * Вспомогательный класс, следящий за изменениями ориентации экрана.
 */
public final class DisplayRotationHelper implements DisplayListener {

    private boolean viewportChanged;
    /**
     * Ширина окна просмотра.
     */
    private int viewportWidth;
    /**
     * Высота окна просмотра.
     */
    private int viewportHeight;
    private final Display display;
    private final DisplayManager displayManager;
    private final CameraManager cameraManager;

    /**
     * Конструктор - создание нового экземпляра. Пописка на события производится отдельно.
     *
     * @param context {@link Context}.
     */
    public DisplayRotationHelper(Context context) {
        displayManager = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
        cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
    }

    /**
     * Регистрирует слушатель {@link DisplayManager}.
     * Должен быть вызван в методе {@link Activity#onResume()}.
     */
    public void onResume() {
        displayManager.registerDisplayListener(this, null);
    }

    /**
     * Отменяет регистрацию слушателя {@link DisplayManager}.
     * Должен быть вызван в методе {@link Activity#onPause()}.
     */
    public void onPause() {
        displayManager.unregisterDisplayListener(this);
    }

    /**
     * Улавливает изменение размеров поверхности.
     * SДолжен быть вызван в методе  {@link android.opengl.GLSurfaceView.Renderer
     * #onSurfaceChanged(javax.microedition.khronos.opengles.GL10, int, int)}**.
     *
     * @param width  Обновленная ширина поверхности.
     * @param height Обновленная высота поверхности.
     */
    public void onSurfaceChanged(int width, int height) {
        viewportWidth = width;
        viewportHeight = height;
        viewportChanged = true;
    }

    /**
     * Обновляет отображение геометрии сессии, если это необходимо.
     * Этот метод должен быть вызван перед каждыми вызовом {@link Session#update()}.
     *
     * @param session Объект {@link Session}, у которого нужно обновить отображение геометрии.
     */
    public void updateSessionIfNeeded(Session session) {
        if (viewportChanged) {
            int displayRotation = display.getRotation();
            session.setDisplayGeometry(displayRotation, viewportWidth, viewportHeight);
            viewportChanged = false;
        }
    }

    @Override
    public void onDisplayAdded(int displayId) {}

    @Override
    public void onDisplayRemoved(int displayId) {}

    @Override
    public void onDisplayChanged(int displayId) {
        viewportChanged = true;
    }

    /**
     * Возвращает ширину окна просмотра.
     *
     * @return Ширина окна просмотра.
     */
    public int getWidth() {
        return viewportWidth;
    }

    /**
     * Возвращает высоту окна просмотра.
     *
     * @return Высота окна просмотра.
     */
    public int getHeight() {
        return viewportHeight;
    }

}
