package com.playground.drawingar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Point;
import com.google.ar.core.PointCloud;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.playground.drawingar.helpers.DisplayRotationHelper;
import com.playground.drawingar.helpers.PermissionHelper;
import com.playground.drawingar.helpers.SnackbarHelper;
import com.playground.drawingar.helpers.TrackingStateHelper;
import com.playground.drawingar.model.Stroke;
import com.playground.drawingar.rendering.BackgroundRenderer;
import com.playground.drawingar.rendering.LineShaderRenderer;
import com.playground.drawingar.rendering.LineUtils;
import com.playground.drawingar.rendering.PointCloudRenderer;
import com.playground.drawingar.rendering.PoseUtils;
import com.thebluealliance.spectrum.SpectrumDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

/**
 * Основной экран приложения, в котором происходит работа с дополненной реальностью.
 */
public class MainActivity extends BaseActivity implements GLSurfaceView.Renderer {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Размер очереди касаний экрана.
     */
    private static final int TOUCH_QUEUE_SIZE = 10;

    /**
     * Расстояние до ближней секущей плоскости.
     */
    private static final float NEAR_CLIP = 0.001f;
    /**
     * Расстояние до дальней секущей плоскости.
     */
    private static final float FAR_CLIP = 100.0f;
    /**
     * Толщина создаваемых линий.
     */
    private static final float LINE_WIDTH = 0.02f;

    /**
     * Управляет состоянием системы AR и жизненным циклом сеанса.
     */
    Session mSession;
    /**
     * Следит за состоянием и изменениями в системе AR.
     */
    Frame mFrame;

    /**
     * Поверхность, на которой происходит отрисовка OpenGL.
     */
    private GLSurfaceView mSurfaceView;

    private ImageButton mClearDrawingButton;
    private ImageButton mUndoButton;
    private FrameLayout mBrushColorPicker;
    private ImageView mBrushColorOval;

    /**
     * Помощник при работе со снекбаром.
     */
    private final SnackbarHelper mMessageSnackbarHelper = new SnackbarHelper();
    /**
     * Помощник при работе со сменой конфигурации экрана.
     */
    private DisplayRotationHelper mDisplayRotationHelper;

    /**
     * Визуализатор видеопотока.
     */
    private final BackgroundRenderer mBackgroundRenderer = new BackgroundRenderer();
    /**
     * Визуализатор множества маркеров.
     */
    private final PointCloudRenderer mPointCloudRenderer = new PointCloudRenderer();
    /**
     * Визуализатор линий.
     */
    private final LineShaderRenderer mLineShaderRenderer = new LineShaderRenderer();

    /**
     * Матрица проекции.
     */
    private float[] projmtx = new float[16];
    /**
     * Матрица вида.
     */
    private float[] viewmtx = new float[16];

    private boolean mUserRequestedARCoreInstall = true;

    /**
     * Координаты последнего касания.
     */
    private Vector2f mLastTouch;
    /**
     * Линия в процессе построения.
     */
    private AtomicBoolean bTouchDown = new AtomicBoolean(false);
    /**
     * Пользователь решил удалить все линии.
     */
    private AtomicBoolean bClearDrawing = new AtomicBoolean(false);
    /**
     * Пользователь отменил последнее действие.
     */
    private AtomicBoolean bUndo = new AtomicBoolean(false);
    /**
     * Размер заполненной очереди касаний экрана.
     */
    private AtomicInteger mTouchQueueSize = new AtomicInteger(0);
    /**
     * Очередь касаний экрана.
     */
    private AtomicReferenceArray<Vector2f> mTouchQueue
            = new AtomicReferenceArray<>(TOUCH_QUEUE_SIZE);

    /**
     * Плоскость, на которую проецируются линии.
     */
    private volatile Pose mPlanePose;

    /**
     * Идентификатор ресурса текущего цвета линий.
     */
    private AtomicInteger bSelectedColor = new AtomicInteger(-1);

    /**
     * Список всех линий.
     */
    List<Stroke> mStrokes = new ArrayList<>();
    /**
     * Новая линия.
     */
    private AtomicBoolean bNewStroke = new AtomicBoolean(false);

    /**
     * onCreate - часть жизненного цикла активности.
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSurfaceView = findViewById(R.id.surface_view);

        mDisplayRotationHelper = new DisplayRotationHelper(this);

        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return handleTouch(event);
            }
        });

        mClearDrawingButton = findViewById(R.id.clear_button);
        mClearDrawingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bClearDrawing.set(true);
                showStrokeDependentUI();
            }
        });

        mBrushColorOval = findViewById(R.id.brush_color_oval);
        mBrushColorPicker = findViewById(R.id.brush_color_picker);
        mBrushColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorDialog();
            }
        });

        mUndoButton = findViewById(R.id.undo_button);
        mUndoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bUndo.set(true);
            }
        });

        mSurfaceView.setPreserveEGLContextOnPause(true);
        mSurfaceView.setEGLContextClientVersion(2);
        mSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        mSurfaceView.setRenderer(this);
        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        mSurfaceView.setWillNotDraw(false);
    }

    /**
     * Показывает диалог с палитрой.
     */
    private void showColorDialog() {
        new SpectrumDialog.Builder(this)
                .setColors(R.array.demo_colors)
                .setSelectedColor(bSelectedColor.get())
                .setDismissOnColorSelected(true)
                .setOutlineWidth(0)
                .setOnColorSelectedListener(new SpectrumDialog.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(boolean positiveResult, @ColorInt int color) {
                        if (positiveResult) {
                            bSelectedColor.set(color);
                            updateBrushColorButton();
                        }
                    }
                }).build().show(getSupportFragmentManager(), "dialog_demo");
    }

    /**
     * Обновляет цвет селектора с палитрой.
     */
    private void updateBrushColorButton() {
        Drawable background = mBrushColorOval.getDrawable();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(bSelectedColor.get());
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(bSelectedColor.get());
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(bSelectedColor.get());
        }
    }

    /**
     * onResume - часть жизненного цикла активности.
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (PermissionHelper.hasCameraPermission(this)) {
            int message = -1;
            Exception exception = null;
            try {
                if (mSession == null) {
                    switch (ArCoreApk.getInstance()
                            .requestInstall(this, mUserRequestedARCoreInstall)) {
                        case INSTALLED:
                            mSession = new Session(this);

                            break;
                        case INSTALL_REQUESTED:
                            // Ensures next invocation of requestInstall() will either return
                            // INSTALLED or throw an exception.
                            mUserRequestedARCoreInstall = false;
                            // at this point, the activity is paused and user will go through
                            // installation process
                            return;
                    }
                }
            } catch (Exception e) {
                exception = e;
                message = getARCoreInstallErrorMessage(e);
            }

            // display possible ARCore error to user
            if (message >= 0) {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                Log.e(TAG, "Exception creating session", exception);
                finish();
                return;
            }

            Config config = new Config(mSession);
            config.setLightEstimationMode(Config.LightEstimationMode.DISABLED);
            if (!mSession.isSupported(config)) {
                Toast.makeText(this, "This device does not support AR", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            mSession.configure(config);

            try {
                mSession.resume();
            } catch (Exception e) {
                Toast.makeText(this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
            }

            mSurfaceView.onResume();
            mDisplayRotationHelper.onResume();
        } else {
            startActivity(new Intent(this, PermissionsActivity.class));
            finish();
        }
    }

    /**
     * onPause - часть жизненного цикла активности.
     */
    @Override
    protected void onPause() {
        super.onPause();

        mDisplayRotationHelper.onPause();
        mSurfaceView.onPause();
        if (mSession != null) {
            mSession.pause();
        }
    }

    /**
     * Часть жизненного цикла {@link GLSurfaceView}.
     * Вызывается при создании поверхности.
     */
    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        try {
            mBackgroundRenderer.createOnGlThread(this);
            mPointCloudRenderer.createOnGlThread(this);
            mLineShaderRenderer.createOnGlThread(this);
            mLineShaderRenderer.bNeedsUpdate.set(true);

        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    /**
     * Часть жизненного цикла {@link GLSurfaceView}.
     * Вызывается при изменении размера поверхности.
     */
    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        mDisplayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    /**
     * Часть жизненного цикла {@link GLSurfaceView}.
     * Вызывается при отрисовки текущего кадра
     */
    @Override
    public void onDrawFrame(GL10 gl10) {
        update();

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        mBackgroundRenderer.draw(mFrame);

        PointCloud pointCloud = mFrame.acquirePointCloud();
        mPointCloudRenderer.update(pointCloud);
        mPointCloudRenderer.draw(viewmtx, projmtx);
        pointCloud.release();

        int width = mDisplayRotationHelper.getWidth();
        int height = mDisplayRotationHelper.getHeight();
        mLineShaderRenderer
                .draw(viewmtx, projmtx, width, height, NEAR_CLIP, FAR_CLIP);
    }

    /**
     * update() выполняется в потоке GL.
     * Метод выполняет все операции, которые необходимо выполнить перед рисованием на экране.
     * Метод:
     * извлекает матрицы проекции и виды;
     * занимается добавлением новых линий и множества маркеров в коллекции;
     * обновляет визуализаторы новыми данными.
     */
    private void update() {
        mDisplayRotationHelper.updateSessionIfNeeded(mSession);

        try {
            mSession.setCameraTextureName(mBackgroundRenderer.getTextureId());

            mFrame = mSession.update();
            Camera camera = mFrame.getCamera();

            if (camera.getTrackingState() == TrackingState.PAUSED) {
                mMessageSnackbarHelper.showMessage(
                        this, TrackingStateHelper.getTrackingFailureReasonString(camera));
                return;
            } else {
                mMessageSnackbarHelper.hide(this);
            }

            mFrame.getCamera().getProjectionMatrix(projmtx, 0, NEAR_CLIP, FAR_CLIP);
            mFrame.getCamera().getViewMatrix(viewmtx, 0);

            boolean needsUpdate = mLineShaderRenderer.bNeedsUpdate.get();
            for (Stroke stroke: mStrokes) {
                if (stroke.finished) {
                    needsUpdate = needsUpdate | stroke.updatePosition();
                }
            }

            mLineShaderRenderer.bNeedsUpdate.set(needsUpdate);

            int numPoints = mTouchQueueSize.get();
            if (numPoints > TOUCH_QUEUE_SIZE) {
                numPoints = TOUCH_QUEUE_SIZE;
            }

            if (numPoints > 0) {
                if (bNewStroke.get()) {
                    bNewStroke.set(false);
                    addStroke(bSelectedColor.get());
                }

                Vector2f[] points = new Vector2f[numPoints];
                for (int i = 0; i < numPoints; i++) {
                    points[i] = mTouchQueue.get(i);
                    mLastTouch = new Vector2f(points[i]);
                }
                addPoint2f(mPlanePose, points);
            }

            // If no new points have been added, and touch is down, add last point again
            if (numPoints == 0 && bTouchDown.get()) {
                addPoint2f(mPlanePose, mLastTouch);
                mLineShaderRenderer.bNeedsUpdate.set(true);
            }

            if (numPoints > 0) {
                mTouchQueueSize.set(0);
                mLineShaderRenderer.bNeedsUpdate.set(true);
            }

            if (bClearDrawing.get()) {
                bClearDrawing.set(false);
                clearDrawing();
                mLineShaderRenderer.bNeedsUpdate.set(true);
            }

            if (!bTouchDown.get()) {
                if (!mStrokes.isEmpty()) {
                    mStrokes.get(mStrokes.size() - 1).finishStroke();
                }
            }

            if (bUndo.get()) {
                bUndo.set(false);
                if (mStrokes.size() > 0) {
                    int index = mStrokes.size() - 1;
                    mStrokes.remove(index);
                    if (mStrokes.isEmpty()) {
                        showStrokeDependentUI();
                    }
                    mLineShaderRenderer.bNeedsUpdate.set(true);
                }
            }

            if (mLineShaderRenderer.bNeedsUpdate.get()) {
                mLineShaderRenderer.mDrawDistance = 0.13f;
                float distanceScale = 0.0f;
                mLineShaderRenderer.setDistanceScale(distanceScale);
                mLineShaderRenderer.setLineWidth(LINE_WIDTH);
                mLineShaderRenderer.clear();
                List<Stroke> strokesToDraw = new ArrayList<>();
                for (Stroke stroke : mStrokes) {
                    if (stroke.isTracking()) strokesToDraw.add(stroke);
                }
                mLineShaderRenderer.updateStrokes(strokesToDraw);
                mLineShaderRenderer.upload();
            }

        } catch (CameraNotAvailableException e) {
            Log.e(TAG, "Camera not available", e);
        }
    }

    /**
     * Удаляет все линии из коллекции и указывает визуализатуру
     * линий удалить все данные и обновиться.
     */
    private void clearDrawing() {
        mStrokes.clear();
        mLineShaderRenderer.clear();
        showStrokeDependentUI();
    }

    /**
     * Обновляет состояние UI элементов.
     */
    private void showStrokeDependentUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mUndoButton.setVisibility(mStrokes.size() > 0 ? View.VISIBLE : View.GONE);
                mClearDrawingButton.setVisibility(mStrokes.size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * Добавляет новую линию в пространство
     * @param color цвет новой линии.
     */
    private void addStroke(@ColorInt int color) {
        Stroke stroke = new Stroke(color, mSession.createAnchor(mPlanePose));
        stroke.setLineWidth(LINE_WIDTH);
        mStrokes.add(stroke);

        showStrokeDependentUI();
    }

    /**
     * Добавляет точку в текущую линию.
     *
     * @param planePose   Плоскость, на которую проецируется точка.
     * @param touchPoints Координаты точек на экране, которые будут проецированы в пространство.
     */
    private void addPoint2f(Pose planePose, Vector2f... touchPoints) {
        Vector3f[] newPoints = new Vector3f[touchPoints.length];
        int width = mDisplayRotationHelper.getWidth();
        int height = mDisplayRotationHelper.getHeight();
        for (int i = 0; i < touchPoints.length; i++) {
            newPoints[i] = LineUtils
                    .GetWorldCoords(touchPoints[i], planePose,
                            width, height, projmtx, viewmtx);
        }
        addPoint3f(newPoints);
    }

    /**
     * Добавляет точку в текущую линию.
     *
     * @param newPoint Координаты точек в мировом пространстве.
     */
    private void addPoint3f(Vector3f... newPoint) {
        int index = mStrokes.size() - 1;

        if (index < 0)
            return;

        for (Vector3f point : newPoint) {
            mStrokes.get(index).add(point);
        }
    }

    /**
     * Обрабатывает нажатия на экран.
     * Здесь происходит поиск плоскости, на которую будет проецировать линия.
     *
     * @param tap Касание экрана в виде объекта класса {@link MotionEvent}.
     */
    private boolean handleTouch(MotionEvent tap) {
        if (mFrame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return false;
        }

        int action = tap.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            for (HitResult hit : mFrame.hitTest(tap)) {
                Trackable trackable = hit.getTrackable();
                if (trackable instanceof Plane) {
                    mPlanePose = hit.getHitPose();
                    mTouchQueue.set(0, new Vector2f(tap.getX(), tap.getY()));

                    mTouchQueueSize.set(1);
                    bTouchDown.set(true);
                    bNewStroke.set(true);

                    break;
                }
                if (trackable instanceof Point) {
                    mPlanePose = PoseUtils.rotatePoseToCamera(mFrame.getCamera(), hit.getHitPose());

                    mTouchQueue.set(0, new Vector2f(tap.getX(), tap.getY()));

                    mTouchQueueSize.set(1);
                    bTouchDown.set(true);
                    bNewStroke.set(true);

                    break;
                }
            }

        } else if (action == MotionEvent.ACTION_MOVE) {
            if (bTouchDown.get()) {
                int numTouches = mTouchQueueSize.addAndGet(1);
                if (numTouches <= TOUCH_QUEUE_SIZE) {
                    mTouchQueue.set(numTouches - 1, new Vector2f(tap.getX(), tap.getY()));
                }
            }
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
            bTouchDown.set(false);
        }

        return true;
    }

}
