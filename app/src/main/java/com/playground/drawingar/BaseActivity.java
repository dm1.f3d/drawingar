package com.playground.drawingar;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

/**
 * Базовый класс для экранов с полноэкранным режимом.
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            setupImmersive();
        }
    }

    /**
     * Конфигурирует экран в полноэкранный режим.
     */
    private void setupImmersive() {
        int visibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        getWindow().getDecorView().setSystemUiVisibility(visibility);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * Возвращает описание ошибки, связанной с ARCore.
     *
     * @param e Исключение от ARCore.
     * @return Идентификатор ресурса с сообщением об ошибке.
     */
    protected int getARCoreInstallErrorMessage(Exception e) {
        if (e instanceof UnavailableArcoreNotInstalledException
                || e instanceof UnavailableUserDeclinedInstallationException) {
            return R.string.install_arcore;
        } else if (e instanceof UnavailableApkTooOldException) {
            return R.string.update_arcore;
        } else if (e instanceof UnavailableSdkTooOldException) {
            return R.string.update_app;
        } else {
            return R.string.ar_not_supported;
        }
    }

}
