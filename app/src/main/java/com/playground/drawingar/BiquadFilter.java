package com.playground.drawingar;

import javax.vecmath.Vector3f;


/**
 * Класс для легкой низкочастотной фильтрации входящих значений.
 * Главная задача – «сглаживать» нарисованные линии.
 */
public class BiquadFilter {

    private BiquadFilterInstance[] inst;

    /**
     * Конструктор - создание нового биквадрического фильтра.
     *
     * @param Fc         The fc
     * @param dimensions The dimensions
     */
    public BiquadFilter(double Fc, int dimensions) {
        inst = new BiquadFilterInstance[dimensions];
        for (int i = 0; i < dimensions; i++) {
            inst[i] = new BiquadFilterInstance(Fc);
        }
    }

    /**
     * Update float.
     *
     * @param in the in
     * @return the float
     */
    public float update(float in) {
        if (inst.length != 1) {
            throw new Error("Expected 1 dimension");
        }

        return (float) inst[0].process(in);
    }

    /**
     * Update vector 3 f.
     *
     * @param in the in
     * @return the vector 3 f
     */
    public Vector3f update(Vector3f in) {
        if (inst.length != 3) {
            throw new Error("Expected 3 dimensions");
        }

        Vector3f val = new Vector3f();
        val.x = (float) inst[0].process(in.x);
        val.y = (float) inst[1].process(in.y);
        val.z = (float) inst[2].process(in.z);
        return val;
    }


    private class BiquadFilterInstance {
        /**
         * The A 0.
         */
        double a0, /**
         * The A 1.
         */
        a1, /**
         * The A 2.
         */
        a2, /**
         * The B 1.
         */
        b1, /**
         * The B 2.
         */
        b2;
        /**
         * The Fc.
         */
        double Fc = 0.5, /**
         * The Q.
         */
        Q = 0.707;
        /**
         * The Z 1.
         */
        double z1 = 0.0,

        /**
         * The Z 2.
         */
        z2 = 0.0;

        /**
         * Instantiates a new Biquad filter instance.
         *
         * @param fc the fc
         */
        BiquadFilterInstance(double fc) {
            Fc = fc;
            calcBiquad();
        }

        /**
         * Process double.
         *
         * @param in the in
         * @return the double
         */
        double process(double in) {
            double out = in * a0 + z1;
            z1 = in * a1 + z2 - b1 * out;
            z2 = in * a2 - b2 * out;
            return out;
        }

        /**
         * Calc biquad.
         */
        void calcBiquad() {
            double norm;
            double K = Math.tan(Math.PI * Fc);
            norm = 1 / (1 + K / Q + K * K);
            a0 = K * K * norm;
            a1 = 2 * a0;
            a2 = a0;
            b1 = 2 * (K * K - 1) * norm;
            b2 = (1 - K / Q + K * K) * norm;
        }
    }
}
